//package com.lvmoney.frame.office.aspose.util;/**
// * 描述:
// * 包名:com.lvmoney.frame.office.aspose.util
// * 版本信息: 版本1.0
// * 日期:2022/3/4
// * Copyright XXXXXX科技有限公司
// */
//
//
//import com.aspose.words.DocSaveOptions;
//import com.aspose.words.Document;
//import com.aspose.words.SaveFormat;
//import com.sun.org.slf4j.internal.Logger;
//import com.sun.org.slf4j.internal.LoggerFactory;
//
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.InputStream;
//
//import com.lvmoney.frame.core.util.FileUtil;
//
///**
// * @describe：
// * @author: lvmoney/XXXXXX科技有限公司
// * @version:v1.0 2022/3/4 16:54
// */
//public class AsposeUtil {
//    private static final Logger LOGGER = LoggerFactory.getLogger(AsposeUtil.class);
//
//    /**
//     * word转pdf
//     *
//     * @param sourceFile:
//     * @param targetFile:
//     * @throws
//     * @return: void
//     * @author: lvmoney /XXXXXX科技有限公司
//     * @date: 2022/3/4 17:06
//     */
//    public static void word2pdf(String sourceFile, String targetFile) {
//
//        File pdfFile = new File(targetFile);
//        try (FileOutputStream os = new FileOutputStream(pdfFile);) {
//            //sourcerFile是将要被转化的word文档
//            Document doc = new Document(sourceFile);
//            //全面支持DOC, DOCX, OOXML, RTF HTML, OpenDocument, PDF, EPUB, XPS, SWF 相互转换
//            doc.save(os, SaveFormat.PDF);
//        } catch (Exception e) {
//            LOGGER.error("word:{}转pdf:{}报错:{}", sourceFile, targetFile, e);
//        }
//    }
//
//    /**
//     * word转pdf
//     *
//     * @param sourceFile:
//     * @param targetFile:
//     * @throws
//     * @return: void
//     * @author: lvmoney /XXXXXX科技有限公司
//     * @date: 2022/3/4 17:26
//     */
//    public static void word2pdf(byte[] sourceFile, String targetFile) {
//        File pdfFile = new File(targetFile);
//        try (FileOutputStream os = new FileOutputStream(pdfFile);) {
//            InputStream inputStream = FileUtil.byte2Input(sourceFile);
//            //sourcerFile是将要被转化的word文档
//            Document doc = new Document(inputStream);
//            //全面支持DOC, DOCX, OOXML, RTF HTML, OpenDocument, PDF, EPUB, XPS, SWF 相互转换
//            doc.save(os, SaveFormat.PDF);
//        } catch (Exception e) {
//            LOGGER.error("word:{}转pdf:{}报错:{}", sourceFile, targetFile, e);
//        }
//    }
//}
